$ ->
  $('.toggle-search-fields').on 'click', ->
    $('#search-fields').toggle()
    false

  $('form').on 'click', '.remove_search_fields', (event) ->
    $(this).closest('.row').remove()
    event.preventDefault()

  $('form').on 'click', '.add_search_fields', (event) ->
    time = new Date().getTime()
    regexp = new RegExp($(this).data('id'), 'g')
    $(this).before($(this).data('fields').replace(regexp, time))
    event.preventDefault()