module Gears
  class MainController < GearsController
    layout 'gears/gears'

    require 'gears/gears_helper'
    include Gears::GearsHelper

    before_action :set_record, only: [:show, :edit, :update, :destroy]

    helper_method :attrs_for_index, :attrs_for_form, :resource, :search_fields

    before_action :authorize!
    before_action :authenticate!

    include Pundit
    rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

    helper_method :current_gears_user_session, :current_gears_user, :authorize!


    respond_to :html
    class << self
      # @param [Object] resource
      def resource(resource)
        cattr_accessor :resource
        self.resource = resource
        self.resource.class_eval do
          def self.policy_class
            "Gears::#{self.to_s.demodulize}Policy".constantize
          end
        end
      end

      # @param [Array] params
      def permit(*params)
        cattr_accessor :permitted_params
        self.permitted_params = params
      end

      # @param [Array] attrs
      def attrs_for_index(*attrs)
        cattr_accessor :attrs_for_index
        self.attrs_for_index = attrs
      end

      # @param [Array] attrs
      def attrs_for_form(*attrs)
        cattr_accessor :attrs_for_form
        self.attrs_for_form = attrs
      end

      def search_fields(*attrs)
        cattr_accessor :search_fields
        self.search_fields = attrs
      end

      def policies(&block)
        cattr_accessor :pundit
        policy_class_name = "#{self.resource.to_s.demodulize}Policy"

        Gears.const_set(policy_class_name, Class.new(Gears::ApplicationPolicy))

        # policy_class = <<-PE
        #   class #{policy_class_name} < Gears::ApplicationPolicy
        #   end
        # PE

        # unless Gears.const_defined?("Gears::#{policy_class_name}")
          eval("self.pundit ||= #{policy_class_name}")
          self.pundit.class_eval &block
        # end
      end

    end

    def index
      @search = resource.ransack(params[:q], search_key: :q)
      @search_route = resource.to_s.index(/Gears::/) ? [@search] : [:gears, @search]
      @collection = @search.result.page params[:page]
      @gears_title = t('gears.index_title', resource_name: resource.model_name.human)
      authorize resource
      @search.build_condition
    end

    def show
      authorize @record
      @gears_title = t('gears.show_title', resource_name: resource.model_name.human, resource_id: @record.id)
    end

    def new
      @record = resource.new
      authorize @record
      @gears_title = t('gears.new_title', resource_name: resource.model_name.human)
    end

    def create
      @record = resource.create(record_params)
      authorize @record
      respond_custom(:gears, @record)
    end

    def edit
      policy_scope resource
      authorize @record
      @gears_title = t('gears.edit_title', resource_name: resource.model_name.human, resource_id: @record.id)
    end

    def update
      authorize @record
      @record.update(record_params)
      respond_custom(:gears, @record)
    end

    def destroy
      authorize @record
      @record.destroy
      respond_custom(:gears, @record)
    end

    # protected

    # @return [Object]
    def set_record
      @record = resource.find(params[:id])
    end

    def record_params
      params.require(resource.model_name.singular).permit(self.permitted_params)
    end

    # @return [Object]
    def resource
      self.resource ? self.resource : raise(StandardError, t('gears.resource_raise'))
    end

    # @return [Array]
    def search_fields
      self.search_fields.length < 0 ? self.search_fields : self.attrs_for_index
    end

    # @return [Array]
    def attrs_for_index
      self.attrs_for_index
    end

    # @return [Array]
    def attrs_for_form
      self.attrs_for_form
    end

    def authenticate!
      self.pundit.new(current_gears_user, @record)
    end

    def pundit_user
      current_gears_user
    end

    private
    def respond_custom(*args)
      args[1].to_s.index(/Gears::/) ? respond_with(args[1]) : respond_with(args[0], args[1])
    end
  end
end