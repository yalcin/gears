class Gears::UserSessionsController < Gears::GearsController
  layout 'layouts/gears/gears_auth'

  def new
    @user = Gears::User.new
  end

  def create
    @user_session = Gears::UserSession.new params.require(:gears_user)
                                        .permit(:login, :password)
    if @user_session.save
      redirect_to gears_root_path
    else
      redirect_to new_gears_user_session_path
    end
  end

  def destroy
    current_gears_user_session.destroy
    redirect_to new_gears_user_session_path
  end
end
