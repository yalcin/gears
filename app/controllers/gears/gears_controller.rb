class Gears::GearsController < ::ApplicationController

  helper_method :user_not_authorized, :current_gears_user, :current_gears_user_session, :authorize!

  def user_not_authorized
    flash[:alert] = "Bu işlemi yapmaya yetkili değilsiniz!"
    redirect_to(gears_root_path)
  end

  def current_gears_user_session
    return @current_gears_user_session if defined?(@current_gears_user_session)
    @current_gears_user_session = Gears::UserSession.find
  end

  def current_gears_user
    return @current_gears_user if defined?(@current_gears_user)
    @current_gears_user = current_gears_user_session && current_gears_user_session.user
  end


  def authorize!
    unless current_gears_user
      session[:return_to] = request.fullpath
      redirect_to new_gears_user_session_path
      return false
    end
  end

end