class Gears::UsersController < Gears::MainController
  resource Gears::User
  permit :email, :login, :name, :role, :password, :password_confirmation


  policies do |policy|
    policy.index do
      user.admin?
    end

    policy.show do
      user.admin?
    end

    policy.newform do
      user.admin?
    end

    policy.create do
      user.admin?
    end

    policy.edit do
      user.admin?
    end

    policy.update do
      user.admin?
    end

    policy.destroy do
      user.admin?
    end
  end
end