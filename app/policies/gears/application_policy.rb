class Gears::ApplicationPolicy
  attr_reader :user, :record

  def initialize(user, record)
    raise Pundit::NotAuthorizedError, "must be logged in" unless user
    @user = user
    @record = record
  end

  def self.index(&block)
    instance_eval do
      define_method :index? do
        instance_eval &block
      end
    end
  end

  def self.show(&block)
    instance_eval do
      define_method :show? do
        instance_eval &block
      end
    end
  end

  def self.create(&block)
    instance_eval do
      define_method :create? do
        instance_eval &block
      end
    end
  end

  def self.newform(&block)
    instance_eval do
      define_method :new? do
        instance_eval &block
      end
    end
  end

  def self.update(&block)
    instance_eval do
      define_method :update? do
        instance_eval &block
      end
    end
  end

  def self.edit(&block)
    instance_eval do
      define_method :edit? do
        instance_eval &block
      end
    end
  end

  def self.destroy(&block)
    instance_eval do
      define_method :destroy? do
        instance_eval &block
      end
    end
  end


  def scope
    Pundit.policy_scope!(user, record.class)
  end

  class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user = user
      @scope = scope
    end

    def resolve
      scope
    end
  end
end

