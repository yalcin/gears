class DatetimePickerInput < SimpleForm::Inputs::Base
  def input(wrapper_options = nil)
    # TODO: i18n uyumlu datetime format
    input_html_options[:value] = @builder.object[attribute_name].to_time.strftime("%d/%m/%Y %H:%M:%S") if @builder.object.try(:[], attribute_name)

    if input_html_options.has_key?(:data)
      input_html_options[:data].merge!({'date-format' => 'DD/MM/YYYY HH:mm:ss'})
    else
      input_html_options[:data] = {'date-format' => 'DD/MM/YYYY HH:mm:ss'}
    end
    linked_to = if input_options.has_key?(:linked_to)
                  { "linked-to" => input_options[:linked_to] }
                else
                  {}
                end

    merged_input_options = merge_wrapper_options(input_html_options, wrapper_options)
    template.content_tag(:div, nil, class: "input-group date #{attribute_name}_datetimepicker", data: {"datetime-picker" => true}.merge!(linked_to)) do
      template.concat @builder.text_field(attribute_name, merged_input_options)
      template.concat content_tag(:span,
                                  content_tag(:span, nil, class: 'fa fa-calendar'),
                                  class: 'input-group-addon')
    end
  end
  Hash.new.shift
end