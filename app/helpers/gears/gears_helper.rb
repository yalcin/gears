module Gears
  module GearsHelper
    def gears_icon(icon)
      "<i class='glyphicon glyphicon-#{icon}'></i>".html_safe
    end

    def bootstrap_class_for flash_type
      { success: "alert-success", error: "alert-danger", alert: "alert-warning", notice: "alert-info" }[flash_type.to_sym] || flash_type.to_s
    end

    def flash_messages(opts = {})
      flash.each do |msg_type, message|
        concat(content_tag(:div, message, class: "alert #{bootstrap_class_for(msg_type)} fade-in") do
                 concat content_tag(:button, 'x', class: "close", data: { dismiss: 'alert' })
                 concat message
               end)
      end
      nil
    end

    def gears_title(title = nil)
      title ? "#{title} | #{Gears.project_name}" : Gears.project_name
    end

    def link_to_add_search_fields(name, f, type)
      new_object = f.object.send "build_#{type}"
      id = "new_#{type}"
      fields = f.send("#{type}_fields", new_object, child_index: id) do |builder|
        render("gears/shared/" + type.to_s + "_fields", f: builder)
      end
      link_to(name, '#', class: "add_search_fields btn btn-info btn-xs", data: {id: id, fields: fields.gsub("\n", "")})
    end
  end
end
