Rails.application.routes.draw do
  gears_for :gears_users
  namespace :gears do
    resources :users
    resource :user_session, only: [:new, :create, :destroy]
    root 'dashboard#index'
  end
end
