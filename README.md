Gears
=====

Kurulum
--------

Gemfile dosyasina
    
`gem 'gears' git: 'https://yalcin@bitbucket.org/yalcin/gears.git'`

`rails g gears:install`

`rake db:migrate`

`rake gears:users:create_admin email=admin@email.com password=password
`

Ayarlar icin `config/initializers/gears.rb`

Scaffold
---------

`rails g gears:scaffold Article title body`


SimpleForm Custom Input
-----------------------

`= f.input :body, as: :wysiwyg`