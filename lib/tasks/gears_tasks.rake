require 'authlogic'
require 'securerandom'

namespace :gears do
  namespace :users do
    desc 'Create admin user'

    task create_admin: :environment do
      unless ENV.include?('email') and ENV.include?('password')
        raise 'eposta adresi ve sifre belirleyin. orn. `rake [task] email=admin@gearscms.io password=sifre'
      end
      admin = Gears::User.new(
          {login: 'admin', name: 'Admin', role: :admin, email: ENV['email'], password: ENV['password'], password_confirmation: ENV['password']}
      )

      unless admin.save
        puts 'Bir sorun olustu, parametreleri ve ayni isimde bir kullanici olmadigini kontrol edin'
      else
        puts "Admin kullanicisi olusturuldu. Kullanici adi: admin, sifre: #{ENV['password']}"
      end
    end
  end
end