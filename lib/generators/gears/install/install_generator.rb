module Gears
  class InstallGenerator < Rails::Generators::Base

    include Rails::Generators::Migration
    source_root File.expand_path('../templates', __FILE__)

    def self.next_migration_number dirname
      if ActiveRecord::Base.timestamped_migrations
        Time.now.utc.strftime("%Y%m%d%H%M%S")
      else
        "%.3d" % (current_migration_number(dirname) + 1)
      end
    end

    def copy_files
      copy_file 'app/views/gears/layouts/_navigation.html.slim', 'app/views/gears/layouts/_navigation.html.slim'
      copy_file 'app/models/gears/user.rb', 'app/models/gears/user.rb'
      copy_file 'app/models/gears/user_session.rb', 'app/models/gears/user_session.rb'
      copy_file 'app/controllers/gears/dashboard_controller.rb', 'app/controllers/gears/dashboard_controller.rb'
      copy_file 'app/assets/javascripts/gears/custom.js', 'app/assets/javascripts/gears/custom.js'
      copy_file 'app/assets/stylesheets/gears/custom.scss', 'app/assets/stylesheets/gears/custom.scss'
      copy_file 'gears.rb', 'config/initializers/gears.rb'
      copy_file "public/robots.txt", "public/robots.txt"

      migration_template 'db/migrate/create_gears_users.rb', 'db/migrate/create_gears_users.rb'
    end
  end
end
