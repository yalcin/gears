class Gears::User < ActiveRecord::Base
  enum role: Gears.roles

  def self.table_name
    self.to_s.gsub("::", "_").tableize
  end

  acts_as_authentic do |c|
    c.validate_email_field = false
    c.transition_from_crypto_providers = [Authlogic::CryptoProviders::Sha512]
    c.crypto_provider = Authlogic::CryptoProviders::SCrypt
  end

  validates_presence_of :login, :name, :email
  validates_uniqueness_of :login
  validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i

end
