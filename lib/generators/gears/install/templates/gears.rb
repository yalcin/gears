Gears.setup do |config|
  config.roles = [:admin, :editor, :commenter, :analytics, :guest]
  config.project_name = "Gears"
  config.contact_email = "change@me.io"
  config.base_url = "http://localhost:3000/"

  # Bootswatch Theme
  config.theme = "united"
end