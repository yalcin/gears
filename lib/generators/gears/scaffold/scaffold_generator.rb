require 'rails/generators/active_record'

module Gears
  class ScaffoldGenerator < Rails::Generators::NamedBase

    include Rails::Generators::Migration

    source_root File.expand_path('../templates', __FILE__)

    argument :attributes, type: :array, banner: 'field:type', required: true

    class_options create_model: true

    def self.next_migration_number dirname
      if ActiveRecord::Base.timestamped_migrations
        Time.now.utc.strftime("%Y%m%d%H%M%S")
      else
        "%.3d" % (current_migration_number(dirname) + 1)
      end
    end

    def create_gears_resource
      @scope = plural_table_name
      @attribute_names = Array.new
      @attribute_names = attributes.map {|attr| attr.reference? ? ":#{attr.name}_id" : ":#{attr.name}"}

      template 'base_controller.rb',     "app/controllers/gears/#{@scope}_controller.rb"
      template 'views/index.html.slim',  "app/views/gears/#{plural_name}/index.html.slim"
      template 'views/show.html.slim',   "app/views/gears/#{plural_name}/show.html.slim"
      template 'views/new.html.slim',    "app/views/gears/#{plural_name}/new.html.slim"
      template 'views/edit.html.slim',   "app/views/gears/#{plural_name}/edit.html.slim"
      template 'views/_form.html.slim',  "app/views/gears/#{plural_name}/_form.html.slim"

      add_to_route
      add_to_navigation rescue nil

      if options[:create_model]
        template 'model.rb', "app/models/#{singular_name}.rb"
        migration_template 'migration.rb', "db/migrate/create_#{plural_name}.rb"
      end
    end

    protected
    def add_to_route
      route "gears_for :#{plural_table_name}"
    end

    def add_to_navigation
      template = "\nli = link_to('#{plural_name.humanize.capitalize}', gears_#{plural_name}_path)"
      File.open('app/views/gears/layouts/_navigation.html.slim', 'a') {
        |file|
        file.write template
      }
    end
  end
end
