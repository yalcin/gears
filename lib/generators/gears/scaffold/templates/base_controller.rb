class Gears::<%= @scope.camelize %>Controller < Gears::MainController
  resource <%= singular_table_name.camelize %>
  permit <%= @attribute_names.join(', ') %>


  policies do |policy|
    policy.index do
      user.admin? || user.editor?
    end

    policy.show do
      user.admin? || user.editor?
    end

    policy.newform do
      user.admin? || user.editor?
    end

    policy.create do
      user.admin? || user.editor?
    end

    policy.edit do
      user.admin? || user.editor?
    end

    policy.update do
      user.admin? || user.editor?
    end

    policy.destroy do
      user.admin? || user.editor?
    end
  end
end