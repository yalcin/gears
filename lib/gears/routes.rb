module ActionDispatch::Routing
  class Mapper
    def gears_for(resource, &block)
      namespace :gears do
        resources resource.to_sym, &block
      end
    end
  end
end