require 'responders'
require 'authlogic'
require 'pundit'
require 'ransack'
require 'cocoon'
require 'kaminari'
require 'slim-rails'
require 'bootstrap-sass'
require 'font-awesome-rails'
require 'summernote-rails'
require 'simple_form'
require 'gears/routes'

module Gears
  class Engine < ::Rails::Engine
    # namespace 'gears'
    # isolate_namespace Gears
    initializer "haber.assets.precompile" do |app|
      app.config.assets.precompile += %w(gears/gears.css gears/gears.js)
    end
  end
end
