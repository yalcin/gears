require "gears/engine"

module Gears
  mattr_accessor :roles
  @@roles = [:admin, :editor, :guest]

  mattr_accessor :project_name
  @@project_name = "Gears"

  mattr_accessor :contact_email
  @@contact_email = nil

  mattr_accessor :base_url
  @@base_url = nil

  mattr_accessor :theme
  @@theme = nil

  def self.setup
    yield self
  end
end
