$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "gears/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "gears"
  s.version     = Gears::VERSION
  s.authors     = ["Mustafa Yalcin Acikyildiz"]
  s.email       = ["y@webliyacelebi.com"]
  s.homepage    = "https://github.com/yalcin/gears"
  s.summary     = "GEARS CMS"
  s.description = "GEARS CMS"
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 4.2.4"
  s.add_dependency "sass-rails", "~> 5.0.3" # SCSS import sorunu
  s.add_dependency 'responders', '~> 2.0'
  s.add_dependency "authlogic", "~> 3.4.5"
  s.add_dependency "ransack", "~> 1.6.6"
  s.add_dependency "pundit"
  s.add_dependency "slim", "~> 3.0.1"
  s.add_dependency "slim-rails", "~> 3.0.1"
  s.add_dependency "kaminari", '~> 0.16.3 '
  s.add_dependency "bootstrap-sass", "~> 3.3.1.0"
  s.add_dependency "font-awesome-rails", "~> 4.3.0.0"
  s.add_dependency "summernote-rails", '~> 0.6.6.0'
  s.add_dependency "cocoon"
  s.add_dependency "simple_form", "~> 3.1.0"

  s.add_development_dependency "sqlite3"
end
