# Preview all emails at http://localhost:3000/rails/mailers/gears/admin_mailer
class Gears::AdminMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/gears/admin_mailer/reset_password_email
  def reset_password_email
    Gears::AdminMailer.reset_password_email
  end

end
